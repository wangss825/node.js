'use strict';
var dataModel = require("./DataModel");
var express = require("express");
var path = require("path");

var modBook = dataModel.book;
var app = express();
var formPath = path.resolve(__dirname, "/");
app.use(express.static(formPath));


const https = require("https");
//url that you will used to retrieve info. Here I use the crossref
const base = "https://api.crossref.org/works/";
const creterion = "10.2196/12121";
const url = base.concat(creterion);
//const url = "https://api.crossref.org/works/10.2196/12121/";
let urlArr = [url];//if you have a lot of url. just added them to this array.

//if we have multiple url. loop through the url to get the info and add to the db.
urlArr.forEach((url, i) => {
  https.get(url, (resp) => {
         let data = '';
         // A chunk of data has been recieved.
         resp.on('data', (chunk) => {
           data += chunk;
         });
         // The whole response has been received.
         resp.on('end', () => {
           //console.log(JSON.parse(data).explanation);
           data = JSON.parse(data);
           //console.log(data);
           //get authors
           let authors = data.message.author;
           let authorArr= [];
           //put author to the authorArr;
           authors.forEach((item, i) => {
             let name = item.given + item.family;
              authorArr.push(name);
           });
           //get publisher
           let publisher = data.message.publisher;
           //get title;
           let title = data.message.title[0];
           //console.log(title);
           //console.log(publisher);
           //loop the db to see if this book has been in there.
           //that means we will not save the same book -no duplicate
           modBook.find(function (err, books) {
               if (err) return console.log(err);
               var bool = false;
               books.forEach(book => {
                 //if the book exist in the db
                   if(book.title == title);
                   bool = true;
               });
               //Normally we would do a bunch of validation stufff here
               //Node.js does have built in validatin modules
               //we will create a new book and add it to the database
               if(!bool) //only the book don't exist in the db will be added to the db
               {
                  modBook.create({ title: title,  author:authorArr,  publisher: publisher });
               }
         });
        }).on("error", (err) => {
         console.log("Error: " + err.message);
       });
  })
});


//when run the app. set the page as the index.html
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

//when the use click the get Books button, retrieve all info from the db
app.get("/getBook", (req, res)=>{
  modBook.find(function(err, books){
    if (err) return console.log(err);
    res.json(books);
    res.end();
  })
})

//set up the content-type and deal with if the page can't be loaded.
app.use((req, res) => {
    res.writeHead(200, { "Content-type": "text/html" });
    res.end("Could not find page");
})



//the port the app use
app.listen(3009);

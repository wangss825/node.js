
var mongoose = require("mongoose");
var mongoDB = "mongodb://127.0.0.1:27017/publications";


mongoose.connect(mongoDB, { useNewUrlParser: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, "mongoDB Connection error"));

//set up new schema for the db.
var bookSchema = mongoose.Schema({
    title: String,
    author: Array,
    publisher: String
});
//export the schema
exports.book = mongoose.model("Books", bookSchema);
